Vue.use(VueMask.VueMaskPlugin);
Vue.directive('mask', VueMask.VueMaskDirective);

const vSelect = {
	props: {
		name: {
			type: String,
			required: true
		},
		label: String,
		items: { 
			required: true
		},
		value: {
			required: true
		},
		fields: Object,

	},
	data () {
		return {
			selectedOption: null,
			opened: false
		}
	},
	computed: {
		currentValueName() {
			if( this.fields ) {
				return this.items.find(item => item.id == this.value).name;
			} else {
				return this.items[value];
			}
		}
	},
	created () {
		this.selectedOption = this.value;
		document.addEventListener('click', this.clickOutside);
	},
	beforeDestroy() {
		document.removeEventListener('click', this.clickOutside);
	},
	watch: {
		value: {
			handler(newValue) {
				this.selectedOption = newValue;
			}
		}
	},
	methods: {
		selectItem(index) {
			this.$emit('input', index);
			this.opened = false;
		},
		clickOutside(e) {
			if (!this.$el.contains(e.target)) {
				this.opened = false;
			}
		}
	},
	template: `
		<div class="form-item v-select">
			<label :for="name" v-if="label">{{ label }}</label>
			<div :class="['jq-selectbox', 'jqselect', {dropdown: opened, opened: opened}]">
				<select
					:id="name" 
					:name="name"
					class="not-styler"
					v-model="selectedOption"
				>
					<option 
						v-for="(option, o) in items"
						:key="fields ? option[fields.id] : o"
						:selected="(fields ? option[fields.id] : o) === value"
						:value="fields ? option[fields.id] : o"
					>
						{{ fields ? option[fields.value] : option }}
					</option>
				</select>
				<div class="jq-selectbox__select" @click="opened = !opened">
					<div class="jq-selectbox__select-text">{{ currentValueName }}</div>
					<div class="jq-selectbox__trigger">
						<div class="jq-selectbox__trigger-arrow"></div>
					</div>
				</div>
				<div class="jq-selectbox__dropdown" v-show="opened">
					<ul>
						<li 
							v-for="(option, o) in items"
							:key="fields ? option[fields.id] : o"
							:class="{selected: (fields ? option[fields.id] : o) === value}"
							@click="selectItem(fields ? option[fields.id] : o)"
						>
							{{ fields ? option[fields.value] : option }}
						</li>
					</ul>
				</div>
			</div>
		</div>`
};

const vTextarea = {
	props: {
		name: {
			type: String,
			required: true
		},
		label: String,
		value: {
			required: true
		},
		placeholder: String,
		error: String
	},
	computed: {
		model: {
			get() {
				return this.value;
			},
			set(value) {
				this.$emit('input', value);
			}
		}
	},
	created () {
		this.selectedOption = this.value;
	},
	mounted() {
		autosize(this.$refs[this.name]);
	},
	methods: {
		resize() {
			setTimeout(() => {
				autosize.update(this.$refs[this.name]);
			}, 250);
		}
	},
	template: `
		<div class="form-item v-textarea">
			<label v-if="label" :for="name">{{ label }}</label>
			<textarea 
				v-bind="$attrs"
				:id="name"
				:name="name"
				:ref="name"
				:placeholder="placeholder"
				@focus="resize"
				@blur="resize"
				v-model="model"
			></textarea>
			<transition name="fade">
				<div v-if="error" class="form-item__error">
					<span>{{ error }}</span>
				</div>
			</transition>
		</div>`
};

const vTextField = {
	props: {
		name: {
			type: String,
			required: true
		},
		label: String,
		type: {
			type: String,
			default: 'text'
		},
		value: {
			required: true
		},
		placeholder: String,
		mask: String
	},
	computed: {
		model: {
			get() {
				return this.value;
			},
			set(value) {
				this.$emit('input', value);
			}
		}
	},
	created () {
		this.selectedOption = this.value;
	},
	template: `
		<div class="form-item v-text-field">
			<label v-if="label" :for="name">{{ label }}</label>
			<input 
				v-if="type !== 'tel'"
				v-bind="$attrs"
				:id="name"
				:name="name"
				:ref="name"
				:type="type"
				:placeholder="placeholder"
				v-model="model"
			>
			<input
				v-else
				v-bind="$attrs"
				:id="name"
				:name="name"
				:ref="name"
				v-mask="'+7 (###) ###-####'"
				:placeholder="placeholder"
				v-model="model"
			>
		</div>`
};

const vDate = {
	props: {
		label: String,
		value: {
			required: true
		},
		min: Date,
		error: String
	},
	computed: {
		model: {
			get() {
				return this.value;
			},
			set(value) {
				if(this.min > value) {
					this.$emit('input', this.min);
				} else {
					this.$emit('input', value);
				}
			}
		}
	},
	template: `
		<div class="form-item">
			<label v-if="label">{{ label }}</label>
			<v-date-picker :mode="'single'" v-model='model' />
			<div v-if="error" class="form-item__error">
				<span>{{ error }}</span>
			</div>
		</div>`
};

new Vue({
	el: '#preorder-conferences',
	components: {
		'v-select': vSelect,
		'v-text-field': vTextField,
		'v-date': vDate,
		'v-textarea': vTextarea,
	},
	data() {
		return {
			parkingPlaces: [],
			typesApartment: [],
			order: {
				apartments: [],
				parkingPlaces: '',
				comeDate:  null,
				leaveDate: null,
				contact: {
					name: '',
					tel: '',
					email: '',
					comment: ''
				},
				total: 0,
			},
		}
	},
	computed: {
		calcTotal() {
			let total = 0;
			for (const apart of this.order.apartments) {
				let apartment = this.typesApartment.find(typeApart => typeApart.id == apart.id);
				let time = apartment.times.find(time => time.id == apart.time);

				total += apartment.price + time.price;
			}

			return total;
		},
		animatedTotal() {
			return this.order.total.toFixed(0);
		}
	},
	created() {
		this.parkingPlaces = parkingPlaces;
		this.typesApartment = typesApartment;
		this.order = {
			...this.order,
			apartments: order.apartments,
			parkingPlaces: order.parkingPlaces
		};
		setTimeout(() => {
			$('#loader').animate({
				opacity: 0
			}, 300, () => {
				$('#loader').css({display: 'none'});
				$('#preorder-conferences').css({height: 'auto', overflow: 'visible'});
				$('#preorder-conferences .preorder-conferences').css({display: 'block', position: 'relative'});
				$('#preorder-conferences .preorder-conferences').animate({
					opacity: 1
				}, 300);
			});
		}, 2000);
	},
	watch: {
		calcTotal: {
			handler(newTotal) {
				TweenLite.to(this.$data.order, 0.5, { total: newTotal });
			}
		},
	},
	methods: {
		setLeaveDate() {
			if( this.order.leaveDate && this.order.leaveDate < this.order.comeDate ) { 
				this.order.leaveDate = this.order.comeDate;
			}
		},
		addApartment() {
			const firstApartment = Object.values(this.typesApartment)[0];
			this.order.apartments.push({ id: firstApartment.id, time: firstApartment.times[0].id }); 
		},
		deleteApartment(i) {
			if(this.order.apartments.length === 1) {
				return 0;
			}
			this.order.apartments.splice(i, 1);
		},
		payOrder() {
			const order = {
				typeOrder: 1,
				apartments: [],
				contact: this.order.contact
			};
			for (const i in this.order.apartments) {
				const orderApartament = this.order.apartments[i];
				const apart = this.typesApartment.find(a => a.id === orderApartament.id);
				const time = apartment.times.find(t => t.id == orderApartament.time).name;

				order.apartments.push({ id: apart.id, name: apart.name, time });
			}
			console.log('You pay order', order);
		}
	}
});