
$(function() {



 

// Бургер меню
$(".burger-menu").click(function(){
  $('.page-menu').fadeToggle(200);
  $('body').addClass('overflow-hidden');
});



// МОБИЛЬНОЕ меню

$('.page-menu__wrap > ul > li.dropdown > span').click(function(){
$(this).parents('ul').toggleClass('opened');
$(this).parents('li').toggleClass('dropdown-opened');

});

$('.page-menu .back').click(function(){
$(this).parents('.dropdown-opened').toggleClass('dropdown-opened').parents('ul').toggleClass('opened');
});


// Закрываем мобильное меню
$('.close-mmenu').click(function() {
  $('body').removeClass('overflow-hidden');
  $('.page-menu').fadeToggle(200);
})
$(document).mouseup(function (e){ // событие клика по веб-документу
  var div = $(".page-menu"); // тут указываем ID элемента
  if (!div.is(e.target) // если клик был не по нашему блоку
      && div.has(e.target).length === 0) { // и не по его дочерним элементам
    div.hide(); // скрываем его
  if ($('body').hasClass('overflow-hidden')) {
    $('body').removeClass('overflow-hidden')
  }
    
  }
});


// КОНЕЦ МОБИЛЬНОГО МЕНЮ






// Скролл по номерах отеля
$(".room-links").on("click","a", function (event) {
    //отменяем стандартную обработку нажатия по ссылке
    event.preventDefault();

    //забираем идентификатор бока с атрибута href
    var id  = $(this).attr('href'),

    //узнаем высоту от начала страницы до блока на который ссылается якорь
      top = $(id).offset().top;
    
    //анимируем переход на расстояние - top за 1500 мс
    $('body,html').animate({scrollTop: top - 90}, 1500);
  });




// FansyBox
 $('.fancybox').fancybox({});


// Сллайдер на главной
let homeSlider = $('.home-slider').slick({
  dots: false,
  arrows: false,
  speed: 300,
  slidesToShow: 1,
  slidesToScroll: 1,
  fade: true,
})

// Слайдер Галереи
$('.gallery-slider').slick({
  dots: false,
  arrows: true,
  speed: 300,
  slidesToShow: 1,
  slidesToScroll: 1,
  fade: true,
  responsive: [{
    breakpoint: 992,
    settings: {
      autoplay: true,
      autoplaySpeed: 5000
    }
  }]
});




// Стилизация селектов
$('select:not(.not-styler)').styler();


// Календарь
$(function () {
    if($('.datetimepicker').length) {
      $('.datetimepicker input').datetimepicker({
          locale: 'ru',
          format: 'DD.MM.YYYY'
  
      });
    }
});

// Подсказка при наведении
$('.question-hover').hover(function() {
  $(this).children('.question-hover__text').stop(true,true).fadeToggle()
})



//Табы
$('.tabs-nav a').click(function() {
    var _id = $(this).attr('href');
    var _targetElement = $('.tabs-wrapper').find('#' + _id);
    $('.tabs-nav a').removeClass('active');
    $(this).addClass('active');
    $('.tabs-item').removeClass('active');
    _targetElement.addClass('active');
    return false;

});


$('.tabs-mobile__btn').click(function() {
    var _targetElementParent = $(this).parent('.tabs-item');
    _targetElementParent.toggleClass('active-m');
    _targetElementParent.find('.tabs-item__body').slideToggle(600);
    $('.tabs-mobile__btn').parent('.tabs-item').removeClass('active');
    return false;
});

// круговое меню на главной

function circleMenu(percent) {
  var $circleMenuCircle = $('#circle-main-menu__sector');
  var $circleMenuRad = +$circleMenuCircle.attr('r');
  var selectedSector = 2 * $circleMenuRad * Math.PI;
  var percentValue = (percent / 100) * selectedSector;

  $circleMenuCircle.css({'stroke-dasharray': percentValue + " " + selectedSector});
}

function selectMenuItem($elem) {
  var percent = +$elem.data('percent');

  $elem.closest('ul').find('li').removeClass('selected')
  $elem.parent().addClass('selected');
  homeSlider.slick('slickGoTo', $elem.parent().index());
  circleMenu(percent);
}

$('.circle-main-menu__list ul a').on('click', function(e) {
  e.preventDefault();
  selectMenuItem($(this));
});

$('.circle-main-menu__button-prev').on('click', function() {
  var countItems = $('.circle-main-menu__list ul li').length;
  var indexSelectedItem = $('.circle-main-menu__list li.selected').index();
  var indexSelectItem = indexSelectedItem - 1 < 0 ? countItems - 1 : indexSelectedItem - 1

  selectMenuItem($('.circle-main-menu__list ul li:nth-child(' + (indexSelectItem + 1) + ') a'));
})

$('.circle-main-menu__button-next').on('click', function() {
  var countItems = $('.circle-main-menu__list ul li').length;
  var indexSelectedItem = $('.circle-main-menu__list li.selected').index();
  var indexSelectItem = indexSelectedItem + 1 === countItems ? 0 : indexSelectedItem + 1

  selectMenuItem($('.circle-main-menu__list ul li:nth-child(' + (indexSelectItem + 1) + ') a'));
})

$(document).ready(function() {
  circleMenu(+$('.circle-main-menu__list ul a:first-child').data('percent'));
  $(".scroll").mCustomScrollbar();
  Stickyfill.add(document.querySelector('header.header'));
})

})