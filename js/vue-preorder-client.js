Vue.use(VueMask.VueMaskPlugin);
Vue.directive('mask', VueMask.VueMaskDirective);

const errorMessages = {
	comeDate: 'Выберите дату заезда',
	leaveDate: 'Выберите дату выезда',
	lowPeopleResidence: 'Количество проживающих не соответствует количеству выбранных мест',
	disabledAddExtraPlace: 'Для выбора дополнительных мест необходимо выбрать все основные спальные места или выбрать на предыдущем шаге одиночное расположение',
};

const vSelect = {
	props: {
		name: {
			type: String,
			required: true
		},
		label: String,
		items: { 
			required: true
		},
		value: {
			required: true
		},
		fields: Object,

	},
	data () {
		return {
			selectedOption: null,
			opened: false
		}
	},
	computed: {
		currentValueName() {
			if( this.fields ) {
				return this.items.find(item => item.id == this.value).name;
			} else {
				return this.items[value];
			}
		}
	},
	created () {
		this.selectedOption = this.value;
		document.addEventListener('click', this.clickOutside);
	},
	beforeDestroy() {
		document.removeEventListener('click', this.clickOutside);
	},
	watch: {
		value: {
			handler(newValue) {
				this.selectedOption = newValue;
			}
		}
	},
	methods: {
		selectItem(index) {
			this.$emit('input', index);
			this.opened = false;
		},
		clickOutside(e) {
			if (!this.$el.contains(e.target)) {
				this.opened = false;
			}
		}
	},
	template: `
		<div class="form-item v-select">
			<label :for="name" v-if="label">{{ label }}</label>
			<div :class="['jq-selectbox', 'jqselect', {dropdown: opened, opened: opened}]">
				<select
					:id="name" 
					:name="name"
					class="not-styler"
					v-model="selectedOption"
				>
					<option 
						v-for="(option, o) in items"
						:key="fields ? option[fields.id] : o"
						:selected="(fields ? option[fields.id] : o) === value"
						:value="fields ? option[fields.id] : o"
					>
						{{ fields ? option[fields.value] : option }}
					</option>
				</select>
				<div class="jq-selectbox__select" @click="opened = !opened">
					<div class="jq-selectbox__select-text">{{ currentValueName }}</div>
					<div class="jq-selectbox__trigger">
						<div class="jq-selectbox__trigger-arrow"></div>
					</div>
				</div>
				<div class="jq-selectbox__dropdown" v-show="opened">
					<ul>
						<li 
							v-for="(option, o) in items"
							:key="fields ? option[fields.id] : o"
							:class="{selected: (fields ? option[fields.id] : o) === value}"
							@click="selectItem(fields ? option[fields.id] : o)"
						>
							{{ fields ? option[fields.value] : option }}
						</li>
					</ul>
				</div>
			</div>
		</div>`
};

const vTextarea = {
	props: {
		name: {
			type: String,
			required: true
		},
		label: String,
		value: {
			required: true
		},
		placeholder: String,
		error: String
	},
	computed: {
		model: {
			get() {
				return this.value;
			},
			set(value) {
				this.$emit('input', value);
			}
		}
	},
	created () {
		this.selectedOption = this.value;
	},
	mounted() {
		autosize(this.$refs[this.name]);
	},
	methods: {
		resize() {
			setTimeout(() => {
				autosize.update(this.$refs[this.name]);
			}, 250);
		}
	},
	template: `
		<div class="form-item v-textarea">
			<label v-if="label" :for="name">{{ label }}</label>
			<textarea 
				v-bind="$attrs"
				:id="name"
				:name="name"
				:ref="name"
				:placeholder="placeholder"
				@focus="resize"
				@blur="resize"
				v-model="model"
			></textarea>
			<transition name="fade">
				<div v-if="error" class="form-item__error">
					<span>{{ error }}</span>
				</div>
			</transition>
		</div>`
};

const vTextField = {
	props: {
		name: {
			type: String,
			required: true
		},
		label: String,
		type: {
			type: String,
			default: 'text'
		},
		value: {
			required: true
		},
		placeholder: String,
		mask: String,
		error: String
	},
	computed: {
		model: {
			get() {
				return this.value;
			},
			set(value) {
				this.$emit('input', value);
			}
		}
	},
	created () {
		this.selectedOption = this.value;
	},
	template: `
		<div class="form-item v-text-field">
			<label v-if="label" :for="name">{{ label }}</label>
			<input 
				v-if="type !== 'tel'"
				v-bind="$attrs"
				:id="name"
				:name="name"
				:ref="name"
				:type="type"
				:placeholder="placeholder"
				v-model="model"
			>
			<input
				v-else
				v-bind="$attrs"
				:id="name"
				:name="name"
				:ref="name"
				type="tel"
				v-mask="'+7 (###) ###-####'"
				:placeholder="placeholder"
				v-model="model"
			>
			<transition name="fade">
				<div v-if="error" class="form-item__error">
					<span>{{ error }}</span>
				</div>
			</transition>
		</div>`
};

const vAmountField = {
	props: {
		name: {
			type: String,
			required: true
		},
		label: String,
		labelMobile: String,
		value: {
			required: true
		},
		min: Number,
		max: Number,
		error: String
	},
	data() {
		return {
			isMobile: false,
		}
	},
	computed: {
		model: {
			get() {
				return +this.value;
			},
			set(value) {
				this.$emit('input', +value);
			}
		}
	},
	created() {
		this.checkMobile();
		window.addEventListener('resize', this.checkMobile);
	},
	beforeDestroy() {
		window.removeEventListener('resize', this.checkMobile);
	},
	methods: {
		checkMobile() {
			this.isMobile = window.matchMedia("(max-width: 768px)").matches;
		},
		remove() {
			if( this.min !== undefined ) {
				if( this.value - 1 < this.min ) {
					this.$emit('input', this.value);
				} else {
					this.$emit('input', this.value - 1);
				}
			} else {
				this.$emit('input', this.value - 1);
			}
		},
		add() {
			if( this.max !== undefined ) {
				if( this.value + 1 > this.max ) {
					this.$emit('input', this.value);
				} else {
					this.$emit('input', this.value + 1);
				}
			} else {
				this.$emit('input', this.value + 1);
			}
		}
	},
	template: `
		<div class="form-item form-item__amount form-item_not-focus">
			<label v-if="label && !isMobile" :for="name">{{ label }}</label>
			<label v-if="labelMobile && isMobile" :for="name">{{ labelMobile }}</label>
			<slot />
			<div class="form-item__amount-field">
				<button class="form-item__amount-remove" :disabled="min === max" @click="remove"></button>
				<input 
					v-bind="$attrs"
					:id="name"
					:name="name"
					:ref="name"
					type="text"
					readonly
					:disabled="min === max"
					v-model="model"
				>
				<button class="form-item__amount-add" :disabled="min === max" @click="add"></button>
			</div>
			<transition name="fade">
				<div v-if="error" class="form-item__error">
					<span>{{ error }}</span>
				</div>
			</transition>
		</div>`
};

const vDate = {
	props: {
		label: String,
		value: {
			required: true
		},
		min: Date,
		error: String
	},
	computed: {
		model: {
			get() {
				return this.value;
			},
			set(value) {
				if(this.min > value) {
					this.$emit('input', this.min);
				} else {
					this.$emit('input', value);
				}
			}
		}
	},
	template: `
		<div class="form-item">
			<label v-if="label">{{ label }}</label>
			<v-date-picker :mode="'single'" v-model='model' />
			<transition name="fade">
				<div v-if="error" class="form-item__error">
					<span>{{ error }}</span>
				</div>
			</transition>
		</div>`
};

new Vue({
	el: '#preorder-room',
	components: {
		'v-select': vSelect,
		'v-text-field': vTextField,
		'v-date': vDate,
		'v-amount-field': vAmountField,
		'v-textarea': vTextarea,
	},
	data() {
		return {
			step: 1,
			errors: {},
			errorMessages: {},
			percent: {},
			apartments: [],
			order: {
				comeDate:  null,
				leaveDate: null,
				contact: {
					name: '',
					tel: '',
					email: '',
					comment: ''
				},
				total: 0,
			}
		}
	},
	created() {
		this.order = {
			...this.order,
			...order
		};
		this.apartments = apartments;
		this.percent = percent;
		this.errorMessages = errorMessages;
		setTimeout(() => {
			$('#loader').animate({
				opacity: 0
			}, 300, () => {
				$('#loader').css({display: 'none'});
				$('#preorder-room').css({height: 'auto', overflow: 'visible'});
				$('#preorder-room .preorder-room-wrap').css({display: 'block', position: 'relative'});
				$('#preorder-room .preorder-room-wrap').animate({
					opacity: 1
				}, 300);
			});
		}, 2000);
	},
	computed: {
		// выбранный номер
		dataSelectedApartment() {
			const apartment = this.apartments.find(a => a.id === this.order.category);
			const apartmentType = apartment.types.find(t => t.id === this.order.type);

			return apartmentType;
		},
		// мин и макс спальных мест в выбранном номере
		currentMinMaxPlaces() {
			return this.dataSelectedApartment.sleepingPlaces;
		},
		// цены выбранного плейса
		currentPrices() {
			return this.dataSelectedApartment.price;
		},
		// количество выбранных спальных мест
		totalSleepingPlace() {
			return this.order.sleepingPlaces.bed
						+	this.order.sleepingPlaces.sofa
						+ this.order.sleepingPlaces.foldingBed;
		},
		// количество людей для распределения по возрастам
		availableCountPeople() {
			return {
				adult: this.totalSleepingPlace - this.order.people.residence.after_16 - this.order.people.residence.between_4_16 - this.order.people.residence.before_4,
				after_16: this.totalSleepingPlace - this.order.people.residence.adult - this.order.people.residence.between_4_16 - this.order.people.residence.before_4,
				between_4_16: this.totalSleepingPlace - this.order.people.residence.adult - this.order.people.residence.after_16 - this.order.people.residence.before_4,
				before_4: this.totalSleepingPlace - this.order.people.residence.adult - this.order.people.residence.after_16 - this.order.people.residence.between_4_16
			}
		}
	},
	watch: {
		'order.sleepingPlaces.bed': {
			handler() {
				if( 'disabledAddSofa' in this.errors) {
					delete this.errors.disabledAddSofa;
				}

				if( 'disabledAddFoldingBed' in this.errors) {
					delete this.errors.disabledAddFoldingBed;
				}
			}
		},
		'order.sleepingPlaces.sofa': {
			handler() {
				if( this.order.sleepingPlaces.bed !== this.dataSelectedApartment.sleepingPlaces.bed.max ) {
					this.errors.disabledAddSofa = this.errorMessages.disabledAddExtraPlace;
				}

				if( !this.order.sleepingPlaces.sofa && 'disabledAddSofa' in this.errors) {
					delete this.errors.disabledAddSofa;
				}
			}
		},
		'order.sleepingPlaces.foldingBed': {
			handler() {
				if( this.order.sleepingPlaces.bed !== this.dataSelectedApartment.sleepingPlaces.bed.max ) {
					this.errors.disabledAddFoldingBed = this.errorMessages.disabledAddExtraPlace;
				}

				if( !this.order.sleepingPlaces.foldingBed && 'disabledAddFoldingBed' in this.errors) {
					delete this.errors.disabledAddFoldingBed;
				}
			}
		},
		'order.comeDate': {
			handler() {
				if ('comeDate' in this.errors) {
					delete this.errors.comeDate;
				}
			}
		},
		'order.leaveDate': {
			handler() {
				if ('leaveDate' in this.errors) {
					delete this.errors.leaveDate;
				}
			}
		},
		'order.people.residence.adult': {
			handler() {
				if( this.order.people.residence.adult < order.people.therapy.adult ) {
					this.order.people.therapy.adult = order.people.residence.adult;
				}
				if ('button' in this.errors) {
					delete this.errors.button;
				}
			}
		},
		'order.people.residence.after_16': {
			handler() {
				if( this.order.people.residence.after_16 < order.people.therapy.after_16 ) {
					this.order.people.therapy.after_16 = order.people.residence.after_16;
				}
				if ('button' in this.errors) {
					delete this.errors.button;
				}
			}
		},
		'order.people.residence.between_4_16': {
			handler() {
				if( this.order.people.residence.between_4_16 < order.people.therapy.between_4_16 ) {
					this.order.people.therapy.between_4_16 = order.people.residence.between_4_16;
				}
				if ('button' in this.errors) {
					delete this.errors.button;
				}
			}
		},
		'order.people.residence.before_4': {
			handler() {
				if ('button' in this.errors) {
					delete this.errors.button;
				}
			}
		},
		'order.people.residence.before_2': {
			handler() {
				if ('button' in this.errors) {
					delete this.errors.button;
				}
			}
		},
	},
	methods: {
		setLeaveDate() {
			if( this.order.leaveDate && this.order.leaveDate < this.order.comeDate ) { 
				this.order.leaveDate = this.order.comeDate;
			}
		},
		prevStep() {
			this.errors = {};
			switch (this.step) {
				case 4: this.goStep3('prev'); break;
					case 3: this.goStep2('prev'); break;
				case 2: this.goStep1(); break;
			}
		},
		nextStep() {
			switch (this.step) {
				case 1: this.goStep2('next'); break;
					case 2: this.goStep3('next'); break;
				case 3: this.goStep4(); break;
			}
		},
		goStep1() {
			// сброс спальных мест в 0
			this.order.sleepingPlaces = {
				bed: 1,
				sofa: 0,
				foldingBed: 0,
				crib: 0
			};
			this.step = 1;
		},
		goStep2(direction) {
			switch (direction) {
				case 'prev':
					// сброс проживающих/лечащихся в 0
					for(type in this.order.people) {
						for (const age in this.order.people[type]) {
							this.order.people[type][age] = 0;
						}
					}
					this.step = 2;
					break;
				case 'next':
					// проверка даты заезда
					if( !this.order.comeDate ) {
						Vue.set(this.errors, 'comeDate', this.errorMessages.comeDate);
					}
					// проверка даты заезда
					if( !this.order.leaveDate ) {
						Vue.set(this.errors, 'leaveDate', this.errorMessages.leaveDate);
					}
					if( !Object.keys(this.errors).length ) {
						this.step = 2;
					}
					break;
			}
		},
		goStep3(direction) {
			switch (direction) {
				case 'prev':
					this.step = 3;
					break;
				case 'next':
					if( !Object.keys(this.errors).length ) {
						this.step = 3;
					}
					break;
			}
		},
		goStep4() {
			const countSelected = Object.values(this.order.people.residence).reduce((sum, val) => sum + val);
			if( countSelected !== this.totalSleepingPlace ) {
				Vue.set(this.errors, 'button', this.errorMessages.lowPeopleResidence);
			}
			if( !Object.keys(this.errors).length ) {
				this.calcTotal();
				this.step = 4;
			}
		},
		calcTotal() {
			const quantityDays = moment(this.order.leaveDate).diff(this.order.comeDate, 'days') + 1;

			const price = {
				...this.currentPrices,
				therapy: this.currentPrices.therapy - this.currentPrices.residence
			};
			const places = {
				main: this.order.sleepingPlaces.bed,
				extra: this.order.sleepingPlaces.sofa + this.order.sleepingPlaces.foldingBed
			}
			let countInMainPlaceNow = {
				adult: 0,
				after_16: 0,
				between_4_16: 0,
				before_4: 0
			};
			let people = this.order.people;
			this.order.total = 0;

			// сразу вычислим людей на основном месте
			for(const type in people.residence) {
				const totalSleepingPlace = people.residence[type];

				if( Object.values(countInMainPlaceNow).reduce((sum, val) => sum + val) === places.main ) break;

				if(totalSleepingPlace > places.main - 1) { // если людей данной группы больше 1 то сразу расположим их на кровати и удалим их из дальнейшего рассчета по жильцам
					this.order.total += places.main * this.percent[type] * price.residence * quantityDays;
					countInMainPlaceNow[type] = places.main;
					break;
				} else if( totalSleepingPlace === 1 ) { // если человек данной группы равно 1 то добавим сумму за этого человека в итог и вычтем из этой группы жильца
					this.order.total += this.percent[type] * price.residence * quantityDays;
					countInMainPlaceNow[type]++;
				}
			}

			// сумма по проживающим без кровати
			for(const type in people.residence) {
				if( type !== 'before_2' ) {
					const totalSleepingPlace = people.residence[type];
					this.order.total += this.percent[`${type}_extra`] * price.residence * (totalSleepingPlace - countInMainPlaceNow[type]) * quantityDays;
				}
			}

			// сумма по лечащимся
			for(const type in people.therapy) {
				const totalSleepingPlace = people.therapy[type];
				this.order.total += price.therapy * totalSleepingPlace * quantityDays;
			}
		},
		payOrder() {
			const order = {
				typeOrder: 0,
				...this.order,
				categoryName: this.apartments.find(a => a.id === +this.order.category).name,
				typeName: this.dataSelectedApartment.name,
			};
			console.log('You pay order', order);
		}
	}
});